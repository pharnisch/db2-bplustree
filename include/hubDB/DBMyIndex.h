#ifndef HUBDB_DBMYINDEX_H
#define HUBDB_DBMYINDEX_H

#include <hubDB/DBIndex.h>
#include <hubDB/DBTypes.h>
#include <hubDB/DBException.h>

using namespace HubDB::Exception;

// Funktion bekannt machen
extern "C" void *createDBMyIndex(int nArgs, va_list ap);

namespace HubDB {
    namespace Index {
        template<typename attr_t>
        class DBMyIndex : public DBIndex {
        public:
            DBMyIndex(DBBufferMgr &bufferMgr,
                      DBFile &file,
                      enum AttrTypeEnum attrType,
                      ModType mode,
                      bool unique);

            ~DBMyIndex();

            string toString(string linePrefix = "") const;

            string leafToString(string linePrefix = "");

            string nodeToString(string linePrefix = "");

            void initializeIndex();

            void find(const DBAttrType &valWrapped, DBListTID &tids);

            void insert(const DBAttrType &val, const TID &tid);

            void remove(const DBAttrType &val, const DBListTID &tid);

            static int registerClass();

        private:
            void unfixBACBs(bool dirty);

            static const uint MAX_TID_PER_ENTRY = 20; // constant for TID amount maximum for non-unique values
            static const BlockNo META_BLOCK_NUMBER = 0;

//            const uint32_t maxValues();

            void setMetaBlockData();

            bool mergeLeaf();

            bool mergeNode();

            bool stealSibling();

            bool stealInnerNodeSibling();

            void mergeRoot();

            bool rebalance();

            bool check_dublicates(attr_t val);

            BlockNo getMetaBlockData();

            void insertInNode(attr_t val, BlockNo blockNewLeaf);

            const uint tidsPerEntry; // Einträge (TIDs) pro Schlüsseleintrag (maximal MAX_TID_PER_ENTRY or 1 when unique): tidsPerEntry
            static LoggerPtr logger;
            BlockNo rootBlockNo;
            uint maxDepth;

            void splitInner();

            void splitLeaf();

            void findTarget(attr_t val);

            void findInsertTarget(attr_t val);

            bool findRemoveTarget(attr_t val, TID tid);

            void resetStackToOnlyContainRoot();

            void print_tree(int depth = 0);

            attr_t insertInNode(const DBAttrType &val, const TID &tid);


            inline string toString(attr_t value);

            inline attr_t unwrap(const DBAttrType &val);

            stack<DBBACB> bacbStack; // Der bacbStack speichert immer den Path der im Baum durchlaufen wird (z.B. von unten nach oben root->innerNode->leaf)

            string printtree = "";

            /*
             * Class for the MetaNode
             * contains the blockNo of the rootNode
             */
            class MetaNode {
            public:
                const DBBACB &block;
                struct data {
                    BlockNo rootBlockNo;
                    uint maxDepth; // immer mitzählen, in welcher ebene man ist, dann weiß man, ob man leaf oder inner nodes initialisieren muss
                };
                // Simple (De)Serialization (taken from Patricks slides)
                data *data_; // uninitialized pointer data_ of type data (struct)
                explicit MetaNode(DBBACB block) :
                        data_((data *) block.getDataPtr()), // set data_ before constructor body starts
                        block(block) // set block member variable of the class
                {
                    // constructor body
                }
            };

            /*
             * Class for the InnerNode
             * contains key values and inTree-pointers (blockNumbers?) to its children
             */
            class InnerNode {
            public:
                constexpr static uint32_t maxValues() {
                    return (STD_BLOCKSIZE - sizeof(BlockNo) - sizeof(uint32_t))
                           / (sizeof(attr_t) + sizeof(BlockNo));
                }

                const DBBACB &block;
                struct data {
                    uint32_t size = 0; // wichtig, um zu wissen, bis wo werte als sinnvoll zu erachten sind!
                    array<attr_t, maxValues()> values;
                    array<BlockNo, maxValues() + 1> children;
                };
                // Simple (De)Serialization (taken from Patricks slides)
                data *data_; // uninitialized pointer data_ of type data (struct)
                explicit InnerNode(DBBACB block) :
                        data_((data *) block.getDataPtr()), // set data_ before constructor body starts
                        block(block) // set block member variable of the class
                {
                    // constructor body
                }
            };

            /*
             * Class for the LeafNode
             * contains values together with lists of TIDs and one inTree-pointer to the next leafNode
             */
            class LeafNode {
            public:
                constexpr static uint32_t maxValues() {
                    return (STD_BLOCKSIZE - sizeof(BlockNo) - sizeof(BlockNo) - sizeof(uint32_t))
                           / (sizeof(attr_t) + sizeof(TID));
                }


                const DBBACB &block;
                struct data {
                    uint32_t size = 0; // wichtig, um zu wissen, bis wo werte als sinnvoll zu erachten sind!
                    array<attr_t, maxValues()> values;
                    //array<array<TID, MAX_TID_PER_ENTRY>, getOrder(sizeof(attr_t))> tids;
                    array<TID, maxValues()> tids;
                    BlockNo next; // linked list of leafs
                    BlockNo previous = 0; // linked list of leafs
                };
                // Simple (De)Serialization (taken from Patricks slides)
                data *data_; // uninitialized pointer data_ of type data (struct)
                explicit LeafNode(DBBACB block) :
                        data_((data *) block.getDataPtr()), // set data_ before constructor body starts
                        block(block) // set block member variable of the class
                {
                    // constructor body
                }
            };
        };

        template<typename attr_t>
        LoggerPtr DBMyIndex<attr_t>::logger(Logger::getLogger("HubDB.Index.DBMyIndex"));

        template<>
        inline string DBMyIndex<int>::toString(int value) {
            return to_string(value);
        }

        template<>
        inline string DBMyIndex<double>::toString(double value) {
            return to_string(value);
        }

        template<>
        inline string DBMyIndex<array<char, MAX_STR_LEN>>::toString(array<char, MAX_STR_LEN> value) {
            return value.data();
        }

        template<>
        inline int DBMyIndex<int>::unwrap(const DBAttrType &_val) {
            if (_val.type() != INT)
                throw HubDB::Exception::DBIndexException("Integer index used with wrong attribute type.");
            return ((const DBIntType &) _val).getVal();
        }

        template<>
        inline double DBMyIndex<double>::unwrap(const DBAttrType &_val) {
            if (_val.type() != DOUBLE)
                throw HubDB::Exception::DBIndexException("Double index used with wrong attribute type.");
            return ((const DBDoubleType &) _val).getVal();
        }

        template<>
        inline array<char, MAX_STR_LEN> DBMyIndex<array<char, MAX_STR_LEN>>::unwrap(const DBAttrType & _val) {
            if (_val.type() != VCHAR)
                throw HubDB::Exception::DBIndexException("Varchar index used with wrong attribute type.");

            string s = ((const DBVCharType&)_val).getVal();
            array<char, MAX_STR_LEN> a{0};
            size_t len = min<size_t>(a.size(), s.size());
            copy(s.begin(), s.begin() + len, a.begin());
            return a;
        }

        /**
         * Konstruktor
         */
        template<typename attr_t>
        DBMyIndex<attr_t>::DBMyIndex(DBBufferMgr &bufferMgr, DBFile &file, enum AttrTypeEnum attrType, ModType mode,
                                     bool unique) :
                DBIndex(bufferMgr, file, attrType, mode, unique), // call base constructor
                tidsPerEntry(unique ? 1 : MAX_TID_PER_ENTRY) // setzt tidsPerEntry auf 1 oder MAX_TID_PER_ENTRY
        {
            if (logger != NULL) {LOG4CXX_INFO(logger, "DBMyIndex()"); }

            if (bufMgr.getBlockCount(file) ==
                0) {//if this function is called for the first time -> index file has 0 blocks
                initializeIndex();
                getMetaBlockData(); // sets member variable of class rootBlockNo and maxDepth (from value of meta block)
            } else {
                getMetaBlockData();
                DBBACB rootBlock = bufMgr.fixBlock(file, rootBlockNo, mode == READ ? LOCK_SHARED : LOCK_EXCLUSIVE);
                bacbStack.push(rootBlock);
            }

            /*  if (logger != NULL) {
                  LOG4CXX_DEBUG(logger, "this:\n" + toString("\t"));
              }*/
        }

        /**
        * Destruktor
        */
        template<typename attr_t>
        DBMyIndex<attr_t>::~DBMyIndex() {
            LOG4CXX_INFO(logger, "~DBMyIndex()");
            unfixBACBs(false);
        }

        /**
         * Freigeben aller vom Index fixierten Blöcke im BufferManager
         * @param setDirty
         */
        template<typename attr_t>
        void DBMyIndex<attr_t>::unfixBACBs(bool setDirty) {
            LOG4CXX_INFO(logger, "unfixBACBs()");
            LOG4CXX_DEBUG(logger, "setDirty: " + TO_STR(setDirty));
            LOG4CXX_DEBUG(logger, "bacbStack.size()= " + TO_STR(bacbStack.size()));
            while (!bacbStack.empty()) {
                try {
                    if (bacbStack.top().getModified()) {
                        if (setDirty) {
                            bacbStack.top().setDirty();
                        }
                    }
                    bufMgr.unfixBlock(bacbStack.top());
                } catch (DBException e) {
                }
                bacbStack.pop();
            }
        }

        /**
         * Ausgabe des Indexes zum Debuggen
         */
        template<typename attr_t>
        string DBMyIndex<attr_t>::toString(string linePrefix) const {
        }


        template<typename attr_t>
        string DBMyIndex<attr_t>::leafToString(string linePrefix) {
            DBBACB block = bacbStack.top();
            LeafNode leaf(block);
            string s = "leaf Block Nr: " + std::to_string(block.getBlockNo()) + "| Size: " +
                       std::to_string(leaf.data_->size) + " | " + " Previous Leaf Block Nr.: " +
                       std::to_string(leaf.data_->previous) + " NextBlock Nr: " + std::to_string(leaf.data_->next) +
                       " | " + " Max Values: " + std::to_string(leaf.maxValues()) + " Values:  |";
            for (int i = 0; i < leaf.data_->size; i++) {
                s += i + " - Value: " + toString(leaf.data_->values[i]) + " TID: " + leaf.data_->tids[i].toString() +
                     " || ";
            }
            s += "\n";
            return s;
        }

        template<typename attr_t>
        string DBMyIndex<attr_t>::nodeToString(string linePrefix) {
            DBBACB block = bacbStack.top();
            InnerNode node(block);
            int size = node.data_->size;
            string s = "innerNode Block Nr: " + std::to_string(block.getBlockNo()) + " | Size: " +
                       std::to_string(size) + " | " + " NextBlock Nr: " +
                       std::to_string(node.data_->children[size]) +
                       "|" + " Max Values: " + std::to_string(node.maxValues()) + " || Values: ";
            for (int i = 0; i < size; i++) {
                s += i + " - Value: " + toString(node.data_->values[i]) + " Block Nr: " +
                     std::to_string(node.data_->children[i]) + " || ";
            }
            s += "\n";
            return s;
        }

        template<typename attr_t>
        void DBMyIndex<attr_t>::print_tree(int depth) {
            if (depth == maxDepth) {
                printtree += leafToString();
            } else {
                DBBACB block = bacbStack.top();
                InnerNode node(block);
                int size = node.data_->size;
                printtree += "\n \n \n";
                printtree += nodeToString();
                for (int i = 0; i <= size; i++) {
                    bacbStack.push(bufMgr.fixBlock(file, node.data_->children[i], LOCK_SHARED));
                    print_tree(depth + 1);
                    bufMgr.unfixBlock(bacbStack.top());
                    bacbStack.pop();
                }
            }
        }


        /**
          * Gibt die Anzahl der Einträge pro Seite zurueck
          *
          * Gesamtblockgroesse: DBFileBlock::getBlockSize()
          *
          * Platz zum Speichern der Position des ersten freien Blocks: sizeof(uint)
          * Laenge des Schluesselattributs: DBAttrType::getSize4Type(attrType)
          * Grösse der TID (siehe DBTypes.h): sizeof(TID)
          *
          * Einträge (TIDs) pro Schlüsseleintrag (maximal MAX_TID_PER_ENTRY or 1 when unique): tidsPerEntry
          *
          * @return
          */
        /*       template<typename attr_t>
               const uint32_t DBMyIndex<attr_t>::maxValues() {
                    return (STD_BLOCKSIZE - sizeof(BlockNo) - sizeof(uint32_t))
                          / (sizeof(attr_t) + sizeof(BlockNo)) - 1; // +1 because of overflow handling
                   //return 5; //small leafs for debugging
               }
       */
        /**
         * Erstellt Indexdatei.
         */
        template<typename attr_t>
        void DBMyIndex<attr_t>::initializeIndex() {
            LOG4CXX_INFO(logger, "initializeIndex()");
            if (bufMgr.getBlockCount(file) != 0) {
                throw DBIndexException("can not initialize existing table");
            }

            try {
                // meta block
                DBBACB metaBlock = bufMgr.fixNewBlock(file);
                MetaNode metaNode(metaBlock);
                rootBlockNo = 1;
                maxDepth = 0;
                setMetaBlockData(); // set rootBlockNo=1 and maxDepth=0 IN the meta block
                bufMgr.unfixBlock(metaBlock);

                // root block
                DBBACB rootBlock = bufMgr.fixNewBlock(file);
                bacbStack.push(rootBlock);
                LeafNode rootNode(rootBlock);
                rootBlock.setModified();
            } catch (DBException e) {
                throw e;
            }
        }

        /**
         * Sucht im Index nach einem bestimmten Wert
         *
         * Die Funktion ändert die übergebene Liste
         * von TID Objekten (siehe DBTypes.h: typedef list<TID> DBListTID;)
         *
         * @param val  zu suchender Schluesselwert
         * @param tids Referenz auf Liste von TID Objekten
         */
        template<typename attr_t>
        void DBMyIndex<attr_t>::find(const DBAttrType &valWrapped, DBListTID &tids) {
            LOG4CXX_INFO(logger, "find()");
            LOG4CXX_DEBUG(logger, "val:\n" + valWrapped.toString("\t"));


            if (bacbStack.size() != 1) {
                throw DBIndexException("Stack hat nicht genau den root block!");
            }

            attr_t val = unwrap(valWrapped);
            // Löschen der übergebenen Liste ("Returnliste")
            tids.clear(); // übernommen von patrick, ka, ob notwendig


            for (int i = 0; i < maxDepth; i++) {
                DBBACB block = bacbStack.top();
                bacbStack.pop();
                InnerNode node(block);
                int size = node.data_->size;
                for (int j = 0; j < size; j++) {
                    if (val < node.data_->values[j]) {
                        DBBACB blockTmp = bufMgr.fixBlock(file, node.data_->children[j], LOCK_SHARED);
                        bacbStack.push(blockTmp);
                        break;
                    }
                    if (j == size - 1) { // case above was never true (take most right child)
                        DBBACB blockTmp = bufMgr.fixBlock(file, node.data_->children[size], LOCK_SHARED);
                        bacbStack.push(blockTmp);
                    }
                }
                bufMgr.unfixBlock(block);
            }

            LeafNode leafNode(bacbStack.top());
            int size = leafNode.data_->size;
            if (unique) {
                for (uint i = 0; i < size; i++) {
                    if (leafNode.data_->values[i] == val) {
                        tids.push_back(leafNode.data_->tids[i]);
                        break;
                    }
                }
            } else {
                throw DBIndexException("non-unique not implemented!");
            }
            while (!bacbStack.empty()) {
                bufMgr.unfixBlock(bacbStack.top());
                bacbStack.pop();
            }
            if (bacbStack.empty()) {
                bacbStack.push(bufMgr.fixBlock(file, rootBlockNo, LOCK_EXCLUSIVE));
            } else {
                throw DBException("Didnt empty stack");
            }
        }

        /**
         * Einfügen eines Schluesselwertes (moeglicherweise bereits vorhangen)
         * zusammen mit einer Referenz auf eine TID.
         * @param val Schlüsselwert
         * @param tid
         */
        template<typename attr_t>
        void DBMyIndex<attr_t>::insert(const DBAttrType &valWrapped, const TID &tid) {
            LOG4CXX_INFO(logger, "insert()");
            LOG4CXX_DEBUG(logger, "val:\n" + valWrapped.toString("\t"));
            LOG4CXX_DEBUG(logger, "tid: " + tid.toString());
            if (mode == READ) {
                throw DBIndexException("try to insert with READ mode");
            }

            attr_t val = unwrap(
                    valWrapped); // beispielsweise von DBInt zu int, weil wir ints speichern wollen und nicht den gesamten Wrapper
            //path = find target leaf L
            findInsertTarget(val);
            if (check_dublicates(val)) {
                throw DBIndexException("try to insert dublicate");
            }

            //add the entry in sorted order
            DBBACB leafBlock = bacbStack.top();
            LeafNode leafNode(leafBlock);
            uint size = leafNode.data_->size;
            if (size == 0) { //Can be the case if its the root block
                leafNode.data_->values[0] = val;
                leafNode.data_->tids[0] = tid;
                leafNode.data_->size = 1;
            } else {
                bool positionFound = false;
                uint position = -1;

                for (uint i = 0; i < size; i++) {// we have only to find out where to insert
                    if (leafNode.data_->values[i] > val) {// insert val at place where first value is greater
                        position = i;
                        positionFound = true;
                        break;
                    }
                }
                if (positionFound) {
                    //von oben nach unten umschichten
                    for (uint i = size; i > position; i--) {
                        leafNode.data_->values[i] = leafNode.data_->values[i - 1];
                        leafNode.data_->tids[i] = leafNode.data_->tids[i - 1];
                    }
                    leafNode.data_->values[position] = val;
                    leafNode.data_->tids[position] = tid;
                    leafNode.data_->size += 1;
                }

                //nicht gefunden, ans ende anhängen (dürfen wir erst nach loop entscheiden)
                if (!positionFound) {
                    leafNode.data_->values[size] = val;
                    leafNode.data_->tids[size] = tid;
                    leafNode.data_->size += 1;
                }
            }
            if (leafNode.data_->size == leafNode.maxValues()) {
                splitLeaf();
                while (!bacbStack.empty()) {
                    DBBACB nodeBlock = bacbStack.top();
                    InnerNode node(nodeBlock);
                    uint size = node.data_->size;
                    if (size == node.maxValues()) {
                        splitInner();
                    } else {
                        bacbStack.pop();
                    }

                }
            } else {
                while (!bacbStack.empty()) {
                    bacbStack.top().setModified();
                    bufMgr.unfixBlock(bacbStack.top());
                    bacbStack.pop();
                }
            }
            bacbStack.push(bufMgr.fixBlock(file, rootBlockNo, LOCK_EXCLUSIVE));
        }

        template<typename attr_t>
        bool DBMyIndex<attr_t>::check_dublicates(attr_t val) {
            LeafNode leafNode(bacbStack.top());
            int size = leafNode.data_->size;
            for (uint i = 0; i < size; i++) {
                if (leafNode.data_->values[i] == val) {
                    return true;
                }
            }
            return false;
        }

        /**
         * Add Value with BlockNumber of Child to Node (without splitting)
         * @tparam attr_t
         * @param val
         * @param blockNewLeaf
         */
        template<typename attr_t>
        void DBMyIndex<attr_t>::insertInNode(attr_t val, BlockNo blockNewLeaf) {
            DBBACB nodeBlock = bacbStack.top();
            InnerNode node(nodeBlock);
            int size = node.data_->size;

            if (size == 0) { //Can be the case if its the root block
                throw DBIndexException("insertInNode: der Fall size==0 wird zuvor im split abgefangen!");
                /*node.data_->values[0] = val;
                node.data_->children[0] = blockNewLeaf;
                node.data_->size = 1;*/
            } else {
                bool positionFound = false;
                uint position = -1;
                for (uint i = 0; i < size; i++) {// we have only to find out where to insert
                    if (node.data_->values[i] > val) {// insert val at place where first value is greater
                        position = i;
                        positionFound = true;
                        break;
                    }
                }
                node.data_->children[size + 1] = node.data_->children[size];
                if (positionFound) {
                    //von oben nach unten umschichten
                    for (uint i = size; i > position; i--) {
                        node.data_->values[i] = node.data_->values[i - 1];
                        node.data_->children[i] = node.data_->children[i - 1];
                    }
                    node.data_->values[position] = val;
                    node.data_->children[position] = blockNewLeaf;
                    node.data_->size += 1;
                }
                //nicht gefunden, ans ende anhängen (dürfen wir erst nach loop entscheiden)
                if (!positionFound) {
                    node.data_->values[size] = val;
                    node.data_->children[size] = blockNewLeaf;
                    node.data_->size += 1;
                }
            }
        }

        /**
         * splittet den obersten block im Stack (funktioniert nur, wenn oberster block ein leaf ist)
         * @tparam attr_t
         */
        template<typename attr_t>
        void DBMyIndex<attr_t>::splitLeaf() {
            //remove current node from path
            DBBACB leafBlock = bacbStack.top();
            LeafNode leafNode(leafBlock);
            bacbStack.pop();
            //split L into two leaf nodes L and L’
            DBBACB leafBlockNew = bufMgr.fixNewBlock(file);
            LeafNode leafNodeNew(leafBlockNew);
            //distribute the keys evenly between the two nodes
            int size = leafNode.data_->size;

            int split_position = size / 2;
            //copy smaller values to new leaf
            for (int i = 0; i < split_position; i++) {
                leafNodeNew.data_->values[i] = leafNode.data_->values[i];
                leafNodeNew.data_->tids[i] = leafNode.data_->tids[i];
            }
            if (maxDepth == 0) {
                //First Split
                leafNodeNew.data_->next = leafBlock.getBlockNo();
                leafNodeNew.data_->previous = META_BLOCK_NUMBER;
                leafNode.data_->previous = leafBlockNew.getBlockNo();
                leafNode.data_->next = META_BLOCK_NUMBER;
            } else {
                leafNodeNew.data_->next = leafBlock.getBlockNo();
                if (leafNode.data_->previous != 0) {
                    DBBACB prevLeafBlock = bufMgr.fixBlock(file, leafNode.data_->previous, LOCK_EXCLUSIVE);
                    LeafNode prevLeaf(prevLeafBlock);
                    prevLeaf.data_->next = leafBlockNew.getBlockNo();

                    leafNodeNew.data_->previous = prevLeafBlock.getBlockNo();
                    leafNode.data_->previous = leafBlockNew.getBlockNo();

                    prevLeafBlock.setModified();
                    bufMgr.unfixBlock(prevLeafBlock);
                }

            }
            leafNodeNew.data_->size = split_position;


            //Copy bigger values to the front
            int j = 0;
            for (int i = split_position; i < size; i++) {
                leafNode.data_->values[j] = leafNode.data_->values[i];
                leafNode.data_->tids[j] = leafNode.data_->tids[i];
                j++;
            }
            leafNode.data_->size = size - leafNodeNew.data_->size;

            //take a copy of the minimum value of L’
            attr_t minimalValueOfNewLeafNode = leafNode.data_->values[0];
            //if path is empty add new root
            if (bacbStack.empty()) {
                DBBACB newRootBlock = bufMgr.fixNewBlock(file);
                bacbStack.push(newRootBlock);
                rootBlockNo = newRootBlock.getBlockNo();
                maxDepth++;
                setMetaBlockData();
            }
            //insert value into the parent node on path
            DBBACB parentBlock = bacbStack.top();
            InnerNode parent(parentBlock);
            size = parent.data_->size;
            if (size == 0) { // new root was just created
                parent.data_->values[0] = minimalValueOfNewLeafNode;
                parent.data_->children[0] = leafBlockNew.getBlockNo();
                parent.data_->children[1] = leafBlock.getBlockNo();
                parent.data_->size = 1;
            } else {
                insertInNode(minimalValueOfNewLeafNode, leafBlockNew.getBlockNo());
            }
            parentBlock.setModified();
            leafBlock.setModified();
            leafBlockNew.setModified();
            bufMgr.unfixBlock(leafBlock);
            bufMgr.unfixBlock(leafBlockNew);
        }

        /**
         * splittet den obersten block im Stack (funktioniert nur, wenn oberster block ein inner node ist)
         * @tparam attr_t
         */
        template<typename attr_t>
        void DBMyIndex<attr_t>::splitInner() {

            //remove current node from path
            DBBACB innerBlock = bacbStack.top();
            InnerNode innerNode(innerBlock);
            bacbStack.pop();
            //split L into two inner nodes L and L’
            //extract the middle value during the split
            //distribute the remaining keys evenly between the two nodes
            DBBACB innerBlockNew = bufMgr.fixNewBlock(file);
            InnerNode innerNodeNew(innerBlockNew);
            int size = innerNode.data_->size;

            int split_position = (size + 1) / 2;
            //copy smaller values to new leaf
            for (int i = 0; i < split_position; i++) {
                innerNodeNew.data_->values[i] = innerNode.data_->values[i];
                innerNodeNew.data_->children[i] = innerNode.data_->children[i];
            }

            //Copy bigger values to the front
            int j = 0;
            for (int i = split_position; i <= size; i++) {
                innerNode.data_->values[j] = innerNode.data_->values[i];
                innerNode.data_->children[j] = innerNode.data_->children[i];
                j++;
            }
            innerNode.data_->size = split_position - 1;
            innerNodeNew.data_->size = size - split_position;

            //take a copy of the minimum value of L’
            attr_t middleValue = innerNodeNew.data_->values[innerNodeNew.data_->size];


            //if path is empty add new root
            if (bacbStack.empty()) {
                DBBACB newRootBlock = bufMgr.fixNewBlock(file);
                bacbStack.push(newRootBlock);
                rootBlockNo = newRootBlock.getBlockNo();
                maxDepth++;
                setMetaBlockData();
            }
            //insert the extracted value into the parent node on path
            DBBACB parentBlock = bacbStack.top();
            InnerNode parent(parentBlock);
            size = parent.data_->size;
            if (size == 0) { // new root was just created
                parent.data_->values[0] = middleValue;
                parent.data_->children[0] = innerBlockNew.getBlockNo();
                parent.data_->children[1] = innerBlock.getBlockNo();
                parent.data_->size = 1;
            } else {
                insertInNode(middleValue, innerBlockNew.getBlockNo());
            }

            parentBlock.setModified();
            innerBlock.setModified();
            innerBlockNew.setModified();
            bufMgr.unfixBlock(innerBlock);
            bufMgr.unfixBlock(innerBlockNew);
        }

        /**
         * füllt den bacbStack mit allen DBBACB blöcken bis zum linkesten Blatt (inklusive), das den val enthält
         */
        template<typename attr_t>
        void DBMyIndex<attr_t>::findTarget(attr_t val) {
            for (int i = 0; i < maxDepth; i++) {
                DBBACB block = bacbStack.top();
                InnerNode node(block);
                int size = node.data_->size;
                for (int j = 0; j < size; j++) {
                    if (node.data_->values[j] > val) {
                        DBBACB blockTmp = bufMgr.fixBlock(file, node.data_->children[j], LOCK_EXCLUSIVE);
                        bacbStack.push(blockTmp);
                        break;
                    }
                    if (j == size - 1) { // case above was never true (take most right child)
                        DBBACB blockTmp = bufMgr.fixBlock(file, node.data_->children[size], LOCK_EXCLUSIVE);
                        bacbStack.push(blockTmp);
                    }
                }
            }
        }

        template<typename attr_t>
        void DBMyIndex<attr_t>::findInsertTarget(attr_t val) {
            if (bacbStack.size() != 1) {
                throw DBIndexException("Stack hat nicht genau den root block!");
            }

            for (int i = 0; i < maxDepth; i++) {
                DBBACB block = bacbStack.top();
                InnerNode node(block);
                int size = node.data_->size;
                for (int j = 0; j < size; j++) {
                    if (node.data_->values[j] > val) {
                        DBBACB blockTmp = bufMgr.fixBlock(file, node.data_->children[j], LOCK_EXCLUSIVE);
                        bacbStack.push(blockTmp);
                        break;
                    }
                    if (j == size - 1) { // case above was never true (take most right child)
                        DBBACB blockTmp = bufMgr.fixBlock(file, node.data_->children[size], LOCK_EXCLUSIVE);
                        bacbStack.push(blockTmp);
                    }
                }
            }
        }

        template<typename attr_t>
        bool DBMyIndex<attr_t>::findRemoveTarget(attr_t val, TID tid) {
            if (bacbStack.size() != 1) {
                throw DBIndexException("Stack hat nicht genau den root block!");
            }

            bool found = false;
            for (int i = 0; i < maxDepth; i++) {
                DBBACB block = bacbStack.top();
                InnerNode node(block);
                int size = node.data_->size;
                for (int j = 0; j < size; j++) {
                    if (node.data_->values[j] > val) {
                        DBBACB blockTmp = bufMgr.fixBlock(file, node.data_->children[j], LOCK_EXCLUSIVE);
                        bacbStack.push(blockTmp);
                        break;
                    }
                    if (j == size - 1) { // case above was never true (take most right child)
                        DBBACB blockTmp = bufMgr.fixBlock(file, node.data_->children[size], LOCK_EXCLUSIVE);
                        bacbStack.push(blockTmp);
                    }
                }
            }
            // check if leaf contains value
            DBBACB blockTmp1 = bacbStack.top();
            LeafNode leaf1(blockTmp1);
            int size = leaf1.data_->size;
            for (int i = 0; i < size; i++) {
                if (leaf1.data_->values[i] == val && leaf1.data_->tids[i] == tid) {
                    return true; // everything is fine, leaf contains value
                }
            }
            return false;
        }

        template<typename attr_t>
        void DBMyIndex<attr_t>::resetStackToOnlyContainRoot() {
            int size = bacbStack.size();
            if (size == 0) {
                throw DBIndexException("stack is empty!");
            } else {
                while (size > 1) {
                    bufMgr.unfixBlock(bacbStack.top());
                    bacbStack.pop();
                    size--;
                }
            }
        }

        /**
         * Entfernt alle Tupel aus der Liste der tids.
         *
         * Um schneller auf der richtigen Seite anfangen zu koennen,
         * wird zum Suchen zusätzlich noch der zu löschende Wert "value" übergeben
         *
         * @param val Der zu löschende Wert
         * @param tid Die Tupel Ids (mindestens eine - mehrere möglich, falls Duplikate erlaubt sind)
         */
        template<typename attr_t>
        void DBMyIndex<attr_t>::remove(const DBAttrType &valWrapped, const list<TID> &tids) {
            LOG4CXX_INFO(logger, "remove()");
            LOG4CXX_DEBUG(logger, "val:\n" + valWrapped.toString("\t"));
            if (mode == READ) {
                throw DBIndexException("try to remove with READ mode");
            }

            if (bacbStack.size() != 1) {
                throw DBIndexException("Stack hat nicht genau den root block!");
            }


            // for (int tidIndex = 0; tidIndex < tids.size(); tidIndex++) { //should use sth like that when duplicates

            attr_t val = unwrap(valWrapped);
            //root is leaf and already in Stack
            bool found_path = findRemoveTarget(val,
                                               tids.front()); // puts path in the bacbStack, but does not check if target is existent
            if (found_path) {
                //remove the entry from L
                DBBACB leafBlock = bacbStack.top();
                LeafNode leafNode(leafBlock);
                bool found = false;
                for (int i = 0; i < leafNode.data_->size - 1; i++) {
                    if (!found && leafNode.data_->values[i] == val && leafNode.data_->tids[i] == tids.front()) {
                        found = true;
                    }
                    if (found) {
                        leafNode.data_->values[i] = leafNode.data_->values[i + 1];
                        leafNode.data_->tids[i] = leafNode.data_->tids[i + 1];
                    }
                }
                leafNode.data_->size = leafNode.data_->size - 1;
                leafBlock.setModified();
                if (maxDepth > 0 && (leafNode.data_->size < leafNode.maxValues() / 2 - 1)) {
                    rebalance();
                } else {
                    bufMgr.unfixBlock(leafBlock);
                    bacbStack.pop();
                }
            }
            while (!bacbStack.empty()) {
                bufMgr.unfixBlock(bacbStack.top());
                bacbStack.pop();
            }
            bacbStack.push(bufMgr.fixBlock(file, rootBlockNo, LOCK_EXCLUSIVE));

            if (!found_path) {
                throw DBIndexException("not found");
            }


        }


        /**
* REbalance des Baumes
*/
        template<typename attr_t>
        bool DBMyIndex<attr_t>::rebalance() {
            //Steal Sibling if possible if not merge
            bool needs_merge = stealSibling();
            if (!needs_merge) {
                mergeLeaf();
                while (!bacbStack.empty()) {
                    DBBACB nodeBlock = bacbStack.top();
                    InnerNode node(nodeBlock);
                    //If its not the root and the size is smaller than half of maxValue rebalance the node
                    if (nodeBlock.getBlockNo() != rootBlockNo && (node.data_->size < node.maxValues() / 2)) {
                        bool needs_merge = stealInnerNodeSibling();
                        if (!needs_merge) mergeNode();
                    }
                    bufMgr.unfixBlock(bacbStack.top());
                    bacbStack.pop();
                }
            } else {
                return true;
            }
        }


        /**
* Merge eines Leafs
*/
        template<typename attr_t>
        bool DBMyIndex<attr_t>::mergeLeaf() {
            DBBACB leafBlock = bacbStack.top();
            LeafNode leafNode(leafBlock);
            bacbStack.pop();
            DBBACB parentBlock = bacbStack.top();
            InnerNode parentNode(parentBlock);
            if (parentBlock.getBlockNo() == rootBlockNo && parentNode.data_->size == 1) {
                mergeRoot();
            } else {
                //find Sibling
                int parent_position = -1;
                for (int i = 0; i <= parentNode.data_->size; i++) {
                    if (parentNode.data_->children[i] == leafBlock.getBlockNo()) {
                        parent_position = i;
                        break;
                    }
                }

                if (parent_position < parentNode.data_->size) {

                    //if leaf is in parent on last position take the smaller leaf
                    DBBACB siblingBlock = bufMgr.fixBlock(file, leafNode.data_->next, LOCK_EXCLUSIVE);
                    LeafNode siblingLeaf(siblingBlock);
                    if (siblingLeaf.data_->size <= siblingLeaf.maxValues() / 2) {
                        bacbStack.push(siblingBlock);
                        //Copy Values of bigger node to the end of leaf
                        int l = leafNode.data_->size;
                        for (int i = 0; i < siblingLeaf.data_->size; i++) {
                            leafNode.data_->values[l] = siblingLeaf.data_->values[i];
                            leafNode.data_->tids[l] = siblingLeaf.data_->tids[i];
                            l++;
                        }
                        DBBACB biggerleaf_block = bufMgr.fixBlock(file, siblingLeaf.data_->next, LOCK_EXCLUSIVE);
                        LeafNode biggerbigger(biggerleaf_block);
                        biggerbigger.data_->previous = leafNode.block.getBlockNo();
                        biggerleaf_block.setModified();
                        bufMgr.unfixBlock(biggerleaf_block);
                        leafNode.data_->size += siblingLeaf.data_->size;
                        leafNode.data_->next = siblingLeaf.data_->next;


                        //Set parent Pointer Block Number of leaf and value of bigger
                        parentNode.data_->values[parent_position] = parentNode.data_->values[parent_position + 1];

                        //copy values to the front
                        for (int i = parent_position + 1; i <= parentNode.data_->size; i++) {
                            parentNode.data_->children[i] = parentNode.data_->children[i + 1];
                            parentNode.data_->values[i] = parentNode.data_->values[i + 1];
                        }
                        parentNode.data_->size--;
                        bufMgr.flushBlock(siblingBlock);
                        bacbStack.pop();

                        parentBlock.setModified();

                        leafBlock.setModified();
                        bufMgr.unfixBlock(leafBlock);

                    }

                } else {
                    DBBACB siblingBlock = bufMgr.fixBlock(file, leafNode.data_->previous, LOCK_EXCLUSIVE);
                    LeafNode siblingLeaf(siblingBlock);
                    if (siblingLeaf.data_->size <= siblingLeaf.maxValues() / 2) {
                        bacbStack.push(siblingBlock);
                        //Copy Values to smaller Leaf

                        int l = siblingLeaf.data_->size;
                        for (int i = 0; i < leafNode.data_->size; i++) {
                            siblingLeaf.data_->values[l] = leafNode.data_->values[i];
                            siblingLeaf.data_->tids[l] = leafNode.data_->tids[i];
                            l++;
                        }

                        DBBACB biggerleaf_block = bufMgr.fixBlock(file, leafNode.data_->next, LOCK_EXCLUSIVE);
                        LeafNode biggerbigger(biggerleaf_block);
                        biggerbigger.data_->previous = siblingLeaf.block.getBlockNo();
                        biggerleaf_block.setModified();
                        bufMgr.unfixBlock(biggerleaf_block);
                        siblingLeaf.data_->size += leafNode.data_->size;
                        siblingLeaf.data_->next = leafNode.data_->next;


                        //Set parent Pointer Block Number of leaf and value of bigger
                        parentNode.data_->values[parent_position - 1] = parentNode.data_->values[parent_position];

                        //copy values to the front
                        for (int i = parent_position; i <= parentNode.data_->size; i++) {
                            parentNode.data_->children[i] = parentNode.data_->children[i + 1];
                            parentNode.data_->values[i] = parentNode.data_->values[i + 1];
                        }
                        parentNode.data_->size--;
                        bufMgr.flushBlock(leafBlock);

                        parentBlock.setModified();

                        siblingBlock.setModified();
                        bacbStack.pop();

                    }
                }
            }
        }

        /**
        * Merge eines Leafs
        */
        template<typename attr_t>
        bool DBMyIndex<attr_t>::stealSibling() {
            DBBACB leafBlock = bacbStack.top();
            LeafNode leafNode(leafBlock);
            bacbStack.pop();
            DBBACB parentBlock = bacbStack.top();
            InnerNode parentNode(parentBlock);
            //find Sibling
            int parent_position = -1;
            for (int i = 0; i <= parentNode.data_->size; i++) {
                if (parentNode.data_->children[i] == leafBlock.getBlockNo()) {
                    parent_position = i;
                    break;
                }
            }

            //if leaf is in parent on last position take the smaller leaf
            bool found_sibling = false;
            bool use_bigger = false;
            if (parent_position < parentNode.data_->size) {
                DBBACB siblingBlock = bufMgr.fixBlock(file, leafNode.data_->next, LOCK_EXCLUSIVE);
                LeafNode siblingLeaf(siblingBlock);
                if (siblingLeaf.data_->size > siblingLeaf.maxValues() / 2) {
                    bacbStack.push(siblingBlock);
                    found_sibling = true;
                    use_bigger = true;
                    //use that sibling
                } else {
                    bufMgr.unfixBlock(siblingBlock);
                }
            }
            if (!found_sibling) {
                if (parent_position > 0) {
                    DBBACB siblingBlock = bufMgr.fixBlock(file, leafNode.data_->previous, LOCK_EXCLUSIVE);
                    LeafNode siblingLeaf(siblingBlock);
                    if (siblingLeaf.data_->size > siblingLeaf.maxValues() / 2) {
                        bacbStack.push(siblingBlock);
                        found_sibling = true;
                        //use that sibling
                    } else {
                        bufMgr.unfixBlock(siblingBlock);
                    }
                }
            }
            if (found_sibling && use_bigger) {
                //Take smallest value of sibling and copy values of sibling to the front
                DBBACB siblingBlock = bacbStack.top();
                LeafNode siblingLeaf(siblingBlock);
                leafNode.data_->values[leafNode.data_->size] = siblingLeaf.data_->values[0];
                leafNode.data_->tids[leafNode.data_->size] = siblingLeaf.data_->tids[0];
                leafNode.data_->size += 1;
                for (int i = 1; i < siblingLeaf.data_->size; i++) {
                    siblingLeaf.data_->values[i - 1] = siblingLeaf.data_->values[i];
                    siblingLeaf.data_->tids[i - 1] = siblingLeaf.data_->tids[i];
                }
                siblingLeaf.data_->size -= 1;
                parentNode.data_->values[parent_position] = siblingLeaf.data_->values[0];
                siblingBlock.setModified();
                bufMgr.unfixBlock(siblingBlock);
                bacbStack.pop();
            } else if (found_sibling) {
                //Take Biggest Values of smaller sibling
                DBBACB siblingBlock = bacbStack.top();
                LeafNode siblingLeaf(siblingBlock);
                //Make Space for smaller value
                for (int i = leafNode.data_->size; i > 0; i--) {
                    leafNode.data_->values[i - 1] = leafNode.data_->values[i];
                    leafNode.data_->tids[i - 1] = leafNode.data_->tids[i];
                }

                leafNode.data_->values[0] = siblingLeaf.data_->values[siblingLeaf.data_->size - 1];
                leafNode.data_->tids[0] = siblingLeaf.data_->tids[siblingLeaf.data_->size - 1];

                //Update own Pointer in parent with new smaller value

                parentNode.data_->values[parent_position - 1] = siblingLeaf.data_->values[siblingLeaf.data_->size - 1];
                siblingLeaf.data_->size -= 1;
                leafNode.data_->size += 1;

                siblingBlock.setModified();
                bufMgr.unfixBlock(siblingBlock);
                bacbStack.pop();
            }

            if (found_sibling) {
                //already set modified and pushed from stack
                bufMgr.unfixBlock(leafBlock);
                //keep Parent Block in Stack for testing if its underflowed
                parentBlock.setModified();

                return true;
            } else {
                bacbStack.push(leafBlock);
                return false; //a merge is needed
            }

        }


        /**
* Merge eines Leafs
*/
        template<typename attr_t>
        bool DBMyIndex<attr_t>::mergeNode() {
            DBBACB nodeBlock = bacbStack.top();
            InnerNode node(nodeBlock);
            bacbStack.pop();
            DBBACB parentBlock = bacbStack.top();
            InnerNode parentNode(parentBlock);
            if (parentBlock.getBlockNo() == rootBlockNo && parentNode.data_->size == 1) {
                mergeRoot();
            } else {
                //find Sibling
                int parent_position = -1;
                for (int i = 0; i <= parentNode.data_->size; i++) {
                    if (parentNode.data_->children[i] == node.block.getBlockNo()) {
                        parent_position = i;
                        break;
                    }
                }

                if (parent_position < parentNode.data_->size) {

                    //if leaf is in parent on last position take the smaller leaf
                    DBBACB siblingBlock = bufMgr.fixBlock(file, parentNode.data_->children[parent_position + 1],
                                                          LOCK_EXCLUSIVE);
                    InnerNode siblingNode(siblingBlock);
                    if (siblingNode.data_->size <= siblingNode.maxValues() / 2) {
                        bacbStack.push(siblingBlock);
                        //Change Value of biggest child (has no value cause its the next child in smaller leaf)

                        node.data_->values[node.data_->size] = parentNode.data_->values[parent_position];
                        node.data_->size++;

                        //Copy Values of bigger node to the end of leaf
                        int l = node.data_->size;
                        for (int i = 0; i <= siblingNode.data_->size; i++) {
                            node.data_->values[l] = siblingNode.data_->values[i];
                            node.data_->children[l] = siblingNode.data_->children[i];
                            l++;
                        }

                        node.data_->size += siblingNode.data_->size;

                        //Set parent Pointer Block Number of leaf and value of bigger
                        parentNode.data_->values[parent_position] = parentNode.data_->values[parent_position + 1];

                        //copy values to the front
                        for (int i = parent_position + 1; i <= parentNode.data_->size; i++) {
                            parentNode.data_->children[i] = parentNode.data_->children[i + 1];
                            parentNode.data_->values[i] = parentNode.data_->values[i + 1];
                        }
                        parentNode.data_->size--;
                        bufMgr.flushBlock(siblingBlock);
                        bacbStack.pop();

                        parentBlock.setModified();

                        nodeBlock.setModified();
                        bufMgr.unfixBlock(nodeBlock);

                    }

                } else {
                    //if leaf is in parent on last position take the smaller leaf
                    DBBACB siblingBlock = bufMgr.fixBlock(file, parentNode.data_->children[parent_position - 1],
                                                          LOCK_EXCLUSIVE);
                    InnerNode siblingNode(siblingBlock);
                    if (siblingNode.data_->size <= siblingNode.maxValues() / 2) {
                        bacbStack.push(siblingBlock);
                        //Copy Values to smaller Leaf
                        int l = siblingNode.data_->size + 1;
                        for (int i = 0; i <= node.data_->size; i++) {
                            siblingNode.data_->values[l] = node.data_->values[i];
                            siblingNode.data_->children[l] = node.data_->children[i];
                            l++;
                        }
                        //Set Value of sibling bigger pointer
                        siblingNode.data_->values[siblingNode.data_->size] = parentNode.data_->values[parent_position -
                                                                                                      1];

                        //Set parent Pointer Block Number of leaf and value of bigger
                        parentNode.data_->values[parent_position - 1] = parentNode.data_->values[parent_position];

                        siblingNode.data_->size += node.data_->size;

                        //copy values to the front
                        for (int i = parent_position; i <= parentNode.data_->size; i++) {
                            parentNode.data_->children[i] = parentNode.data_->children[i + 1];
                            parentNode.data_->values[i] = parentNode.data_->values[i + 1];
                        }
                        parentNode.data_->size--;
                        bufMgr.flushBlock(nodeBlock);

                        parentBlock.setModified();

                        siblingBlock.setModified();
                        bacbStack.pop();

                    }
                }
            }
        }

        /**
 * Merge eines Leafs
 */
        template<typename attr_t>
        void DBMyIndex<attr_t>::mergeRoot() {
            // Check if depth is 1 (we have leaf children)
            // when node children
            //          Take the Childs
            //          Copy Values in smaller one with respect to the next pointer
            //          delete bigger child
            //          delete root
            //          set meta block rootBlockNr, Depth
            // when node leaf
            //          Take the childs
            //          Copy Values in smaller one
            //          Set prev and next to 0
            //          delete bigger child and root
            //          set meta block rootBlockNr, Depth
            if (maxDepth == 1) {
                DBBACB rootBlock = bacbStack.top();
                InnerNode rootNode(rootBlock);
                bacbStack.pop();
                DBBACB smallerChildBlock = bufMgr.fixBlock(file, rootNode.data_->children[0], LOCK_EXCLUSIVE);
                LeafNode smallerLeaf(smallerChildBlock);
                DBBACB biggerChildBlock = bufMgr.fixBlock(file, rootNode.data_->children[1], LOCK_EXCLUSIVE);
                LeafNode biggerLeaf(biggerChildBlock);

                int l = smallerLeaf.data_->size;
                for (int i = 0; i < biggerLeaf.data_->size; i++) {
                    smallerLeaf.data_->values[l] = biggerLeaf.data_->values[i];
                    smallerLeaf.data_->tids[l] = biggerLeaf.data_->tids[i];
                    l++;
                }
                smallerLeaf.data_->size += biggerLeaf.data_->size;
                smallerLeaf.data_->next = 0;
                smallerLeaf.data_->previous = 0;
                bufMgr.flushBlock(biggerChildBlock);
                bufMgr.flushBlock(rootBlock);
                rootBlockNo = smallerChildBlock.getBlockNo();
                maxDepth = 0;
                setMetaBlockData();
                smallerChildBlock.setModified();
                bufMgr.unfixBlock(smallerChildBlock);
            } else {
                DBBACB rootBlock = bacbStack.top();
                InnerNode rootNode(rootBlock);
                bacbStack.pop();
                DBBACB smallerChildBlock = bufMgr.fixBlock(file, rootNode.data_->children[0], LOCK_EXCLUSIVE);
                InnerNode smallerNode(smallerChildBlock);
                DBBACB biggerChildBlock = bufMgr.fixBlock(file, rootNode.data_->children[1], LOCK_EXCLUSIVE);
                InnerNode biggerNode(biggerChildBlock);
                smallerNode.data_->values[smallerNode.data_->size] = rootNode.data_->values[0];
                int l = smallerNode.data_->size + 1;
                for (int i = 0; i <= biggerNode.data_->size; i++) {
                    smallerNode.data_->values[l] = biggerNode.data_->values[i];
                    smallerNode.data_->children[l] = biggerNode.data_->children[i];
                    l++;
                }
                smallerNode.data_->size += biggerNode.data_->size + 1;
                bufMgr.flushBlock(biggerChildBlock);
                bufMgr.flushBlock(rootBlock);
                rootBlockNo = smallerChildBlock.getBlockNo();
                maxDepth--;
                setMetaBlockData();
                smallerChildBlock.setModified();
                bufMgr.unfixBlock(smallerChildBlock);
            }
            DBBACB newRoot = bufMgr.fixBlock(file, rootBlockNo, LOCK_EXCLUSIVE);
            bacbStack.push(newRoot);

        }


        /**
        * Merge eines Leafs
        */
        template<typename attr_t>
        bool DBMyIndex<attr_t>::stealInnerNodeSibling() {
            DBBACB nodeBlock = bacbStack.top();
            InnerNode node(nodeBlock);
            bacbStack.pop();
            DBBACB parentBlock = bacbStack.top();
            InnerNode parentNode(parentBlock);
            //find Sibling
            int parent_position = -1;
            for (int i = 0; i <= parentNode.data_->size; i++) {
                if (parentNode.data_->children[i] == node.block.getBlockNo()) {
                    parent_position = i;
                    break;
                }
            }

            //if leaf is in parent on last position take the smaller leaf
            bool found_sibling = false;
            bool use_bigger = false;
            if (parent_position < parentNode.data_->size) {
                DBBACB siblingBlock = bufMgr.fixBlock(file, parentNode.data_->children[parent_position + 1],
                                                      LOCK_EXCLUSIVE);
                InnerNode siblingNode(siblingBlock);
                if (siblingNode.data_->size > siblingNode.maxValues() / 2) {
                    bacbStack.push(siblingBlock);
                    found_sibling = true;
                    use_bigger = true;
                    //use that sibling
                } else {
                    bufMgr.unfixBlock(siblingBlock);
                }
            }
            if (!found_sibling) {
                if (parent_position > 0) {
                    DBBACB siblingBlock = bufMgr.fixBlock(file, parentNode.data_->children[parent_position - 1],
                                                          LOCK_EXCLUSIVE);
                    InnerNode siblingNode(siblingBlock);
                    if (siblingNode.data_->size > siblingNode.maxValues() / 2) {
                        bacbStack.push(siblingBlock);
                        found_sibling = true;
                        //use that sibling
                    } else {
                        bufMgr.unfixBlock(siblingBlock);
                    }
                }
            }
            if (found_sibling && use_bigger) {
                //Take smallest value of sibling and copy values of sibling to the front
                DBBACB siblingBlock = bacbStack.top();
                InnerNode siblingNode(siblingBlock);


                node.data_->values[node.data_->size] = parentNode.data_->values[parent_position];
                parentNode.data_->values[parent_position] = siblingNode.data_->values[0];
                node.data_->children[node.data_->size + 1] = siblingNode.data_->children[0];
                node.data_->size += 1;

                for (int i = 1; i <= siblingNode.data_->size; i++) {
                    siblingNode.data_->values[i - 1] = siblingNode.data_->values[i];
                    siblingNode.data_->children[i - 1] = siblingNode.data_->children[i];
                }
                siblingNode.data_->size -= 1;
                siblingBlock.setModified();
                bufMgr.unfixBlock(siblingBlock);
                bacbStack.pop();
            } else if (found_sibling) {
                //Take Biggest Values of smaller sibling
                DBBACB siblingBlock = bacbStack.top();
                InnerNode siblingNode(siblingBlock);
                //Make Space for smaller value
                node.data_->children[node.data_->size + 1] = node.data_->children[node.data_->size];
                for (int i = node.data_->size + 1; i > 0; i--) {
                    node.data_->values[i - 1] = node.data_->values[i];
                    node.data_->children[i - 1] = node.data_->children[i];
                }
                node.data_->size++;

                node.data_->children[0] = siblingNode.data_->children[siblingNode.data_->size];
                node.data_->values[0] = parentNode.data_->values[parent_position];

                parentNode.data_->values[parent_position - 1] = siblingNode.data_->values[siblingNode.data_->size -
                                                                                          1];
                siblingNode.data_->size--;

                siblingBlock.setModified();
                bufMgr.unfixBlock(siblingBlock);
                bacbStack.pop();
            }

            if (found_sibling) {
                //already set modified and pushed from stack
                bufMgr.unfixBlock(nodeBlock);
                //keep Parent Block in Stack for testing if its underflowed
                parentBlock.setModified();

                return true;
            } else {
                bacbStack.push(nodeBlock);
                return false; //a merge is needed
            }

        }


        template<typename attr_t>
        void DBMyIndex<attr_t>::setMetaBlockData() {
            DBBACB metaBlock = bufMgr.fixBlock(file, META_BLOCK_NUMBER, LOCK_EXCLUSIVE);
            MetaNode metaNode(metaBlock);
            metaNode.data_->rootBlockNo = rootBlockNo; //metaNode.data_ is a pointer to the block (that is cast into our defined struct)
            metaNode.data_->maxDepth = maxDepth;
            metaBlock.setModified();
            bufMgr.unfixBlock(metaBlock);
        }

        template<typename attr_t>
        BlockNo DBMyIndex<attr_t>::getMetaBlockData() {
            DBBACB metaBlock = bufMgr.fixBlock(file, META_BLOCK_NUMBER, LOCK_EXCLUSIVE);
            MetaNode metaNode(metaBlock);
            rootBlockNo = metaNode.data_->rootBlockNo; // member variable (wird zur laufzeit verwendet) auf den wert setzen, der im metablock drin steht
            maxDepth = metaNode.data_->maxDepth;
            bufMgr.unfixBlock(metaBlock);
        }

/**
* Fügt createDBMyIndex zur globalen Factory-Method-Map hinzu
*/
        template<typename attr_t>
        int DBMyIndex<attr_t>::registerClass() {
            setClassForName("DBMyIndex",
                            createDBMyIndex);
            return 0;
        }
    }
}


#endif //HUBDB_DBMYINDEX_H
