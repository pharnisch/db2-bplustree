#include <hubDB/DBMyIndex.h>
#include <hubDB/DBException.h>

/**
 * Die Implementierung eines Index für Aufgabe 2.
 */

using namespace HubDB::Index;
using namespace HubDB::Exception;

// registerClass()-Methode am Ende dieser Datei: macht die Klasse der Factory bekannt
int rMyIdx = DBMyIndex<int>::registerClass();

// Funktion bekannt machen
extern "C" void *createDBMyIndex(int nArgs, va_list ap);

/**
 * Wird aufgerufen von HubDB::Types::getClassForName von DBTypes, um DBIndex zu erstellen
 * @param DBBufferMgr *: Buffermanager
 * @param DBFile *: Dateiobjekt
 * @param attrType: Attributtp
 * @param ModeType: READ, WRITE
 * @param bool: unique Indexattribut
 */
extern "C" void *createDBMyIndex(int nArgs, va_list ap) {
  // Genau 5 Parameter
  if (nArgs != 5) {
    throw DBException("Invalid number of arguments");
  }
  DBBufferMgr *bufMgr = va_arg(ap, DBBufferMgr *);
  DBFile *file = va_arg(ap, DBFile *);
  enum AttrTypeEnum attrType = (enum AttrTypeEnum) va_arg(ap, int);
  ModType m = (ModType) va_arg(ap, int); // READ, WRITE
  bool unique = (bool) va_arg(ap, int);

  if(attrType == INT){
      return new DBMyIndex<int>(*bufMgr, *file, attrType, m, unique);
  } else if(attrType == DOUBLE){
      return new DBMyIndex<double>(*bufMgr, *file, attrType, m, unique);
  } else if(attrType == VCHAR){
      return new DBMyIndex<array<char, MAX_STR_LEN>>(*bufMgr, *file, attrType, m, unique);
  } else {
      throw DBException("Invalid attribute type");
  }
}